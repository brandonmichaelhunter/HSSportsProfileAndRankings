from loguru import logger

class ErrorLogger(object):
      def __init__(self):
          logger.add("file_{time}.log", format="{time} {level} {message}", rotation="500 MB")  

      
      def LogMessage(self, Message, MethodName):
          logger.error("{} - {}".format(MethodName, Message))


      def InfoMessage(self, Message):
          logger.info("{}".format(Message))