import json
class ConfigService(object):
      Environment: str
      UserName: str
      Password: str
      CollectionName: str
      ServerName: str
      DatabaseName: str
      ConfigData: json
      def __init__(self):
          config = ""
          with open('ETL/ConfigService.json', 'r') as data:
               config = json.load(data)
          
          ConfigData = config[config['Environment']][0]
          self.UserName = ConfigData['usr']
          self.Password = ConfigData['pwd']
          self.ServerName = ConfigData['server']
          self.CollectionName = ConfigData['collection_name']
          self.DatabaseName = ConfigData['database_name']
          


#ConfigData = ConfigService()
#print(ConfigData)