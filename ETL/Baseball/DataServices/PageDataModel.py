from dataclasses import dataclass

@dataclass
class PageDataModel:
      Url: str
      Year: int
      NumberOfPages: int
      Active: bool
    #   def __init__(self, url, year, numOfPages):
    #       self.Url = url
    #       self.Year = year
    #       self.NumberOfPages = numOfPages