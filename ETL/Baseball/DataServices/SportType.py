from enum import Enum

class SportType(Enum):
    BASEBALL = 1
    FOOTBALL = 2
    BASKETBALL = 3
    HOCKEY = 4
    SOCCER = 5