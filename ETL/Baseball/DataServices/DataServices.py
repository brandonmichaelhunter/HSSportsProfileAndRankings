
import pymongo
import json
from json import JSONEncoder
from typing import List

import DataServices as ConfigService
import DataServices as PageDataModel
import DataServices as SportType
import DataServices as ErrorLogger

class DataServices(object):
      
      AppConfig: object
      MONGO_CLIENT_URL: str
      Logger:ErrorLogger
      def __init__(self):
         self.AppConfig = ConfigService.ConfigService()
         self.MONGO_CLIENT_URL = "mongodb://{}:{}@{}/{}".format(self.AppConfig.UserName, self.AppConfig.Password, self.AppConfig.ServerName, self.AppConfig.CollectionName)
         self.Logger = ErrorLogger.ErrorLogger()
        
      def LoadBaseballPagingData(self):
          try:  
          
                client = pymongo.MongoClient(self.MONGO_CLIENT_URL)
                
                db = client["HSSPORTSDB"]
                col = db["BaseballPagingData"]
                
                pagingData = []
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2002, 2, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2003, 2, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2004, 35, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2005, 45, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2006, 45, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2007, 69, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2008, 68, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2009, 82, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2010, 11, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2011, 14, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2012, 18, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2013, 20, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2014, 20, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2015, 25, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2016, 30, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2017, 32, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2018, 38, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2019, 41, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2020, 41, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2021, 44, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2022, 1, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2023, 1, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2024, 1, True))
                pagingData.append(PageDataModel("https://www.perfectgame.org/rankings/Players/NationalRankings.aspx?gyear=", 2025, 1, True))
                for data in pagingData:
                    item = {'Url': data.Url, 'Year': data.Year, 'NumberOfPages': data.NumberOfPages, 'Active': data.Active}
                    self.Logger.InfoMessage(item)
                    col.insert_one(item)
          except Exception as e:
                 self.Logger.LogMessage(e,"LoadBaseballPagingData")

      def GetBaseballPagingDataBySport(self):

          retList = []
          try:
              client = pymongo.MongoClient(self.MONGO_CLIENT_URL)
              db = client["HSSPORTSDB"]
              col = db["BaseballPagingData"]
          
              dataPages = list(col.find({"Active": { "$eq": True}}))
              for data in dataPages:
                  retList.append(PageDataModel.PageDataModel(data["Url"], data["Year"], data["NumberOfPages"], data["Active"]))
          
              return retList
          except Exception as e:
                 self.Logger.LogMessage(e,"GetBaseballPagingDataBySport")
                 return retList
          
      

      def SaveBaseballData(self,Data:object):
          # Save data here.
          try:
              client = pymongo.MongoClient(self.MONGO_CLIENT_URL)
              db = client["HSSPORTSDB"]
              col = db["BaseballPlayerRankings"]
              for item in Data:
                  for row in item:
                      item = {'rank':row.rank,'name':row.name, 'position':row.position, 'hometown':row.hometown, 'city':row.city, 'state':row.state, 'commitment':row.commitment, 'drafted':row.drafted, 'debuted':row.debuted, 
                          'highschool':row.highschool, 'travelteam':row.travelteam, 'description':row.description,'height':row.height, 'weight':row.weight, 'bats':row.bats, 
                          'throw':row.throw, 'year':row.year}
                      self.Logger.InfoMessage(item)
                      col.insert_one(item)
              return True
          except Exception as e:
                 self.Logger.LogMessage(e,"GetBaseballPagingDataBySport")
                 return False
          



