 
from DataServices.DataServices import DataServices
#from DataSvc.DataServices import DataServices
from bs4 import BeautifulSoup
import scrapy
import requests
import BaseballEntity
import mechanize
import re
from time import sleep
import logging
import time
import datetime
import json
browserRequestHeaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]
parserType = 'lxml' #'html.parser'

def GetNumberOfPagesToPage(BaseURL, RequestHeaders): 
    numberPagesToScrape = 0
    br = mechanize.Browser()
    br.addheaders = RequestHeaders #[('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]
    response = br.open(BaseURL)

    navigations = BeautifulSoup(response, parserType)
    pages = []
    for link in navigations("a"):
        if 'href' in link.attrs:
            if "javascript:__doPostBack('ctl00$ContentPlaceHolder1$gvPlayers'" in link.attrs['href']:
                if link.text not in pages:
                    pages.append(link.text)

    numberPagesToScrape = len(pages)            
    return numberPagesToScrape
                
def ParseBaseballData(HtmlPageObj, ProcessingYear):
    #soup = BeautifulSoup(HtmlPage, parserType)
    soup = HtmlPageObj
        #ParseBaseballData(soup)

    playersTable = soup.find('table', attrs={'id':'ContentPlaceHolder1_gvPlayers'})

    rows = playersTable.find_all('tr')
    data = []
    counter = 0
    for row in rows:
        if counter > 1:
           cols = row.find_all('td')
           cols = [ele.text.strip() for ele in cols]
           data.append([ele for ele in cols]) # Get rid of empty values
        else:
            counter += 1

    playerRankingsList = []
    for x in data[1::2]:  
        if(len(x) > 8 and '< First' not in x[0]):
           try:
            print(x)
            
            rank          = x[0]
            playerName    = x[1].split('\n')[0]
            height = ""
            weight = ""
            bat = ""
            throw = ""
            
            if len(x[1].split('\n')) > 1:
                if len(x[1].split('\n')) == 2:
                    height    = x[1].split('\n')[2].split('\xa0')[0]
                    weight    = x[1].split('\n')[2].split('\xa0')[2]
                    bat       = x[1].split('\n')[2].split('\xa0')[4].split('/') [0]
                    throw     = x[1].split('\n')[2].split('\xa0')[4].split('/') [1]
                elif len(x[1].split('\n')) == 3:
                        height  = x[1].split('\n')[2].split('\xa0')[0]
                        if len(x[1].split('\n')[2].split('\xa0')) > 1:
                            weight  = x[1].split('\n')[2].split('\xa0')[2]
                        if(len(x[1].split('\n')[2].split('\xa0')) == 5):
                           bat     = x[1].split('\n')[2].split('\xa0')[4].split('/') [0]
                           throw   = x[1].split('\n')[2].split('\xa0')[4].split('/') [1]
                        else:
                            bat = ""
                            throw = ""
                #elif len(x[1].split('\n')[2].split('\xa0')) == 5:
                #     bat     = x[1].split('\n')[2].split('\xa0')[4].split('/')[0]
                #     throw   = x[1].split('\n')[2].split('\xa0')[4].split('/')[1]
            position      = x[2]
            hometown      = x[3]
            city          = x[3].split(',')[0].strip()
            state         = x[3].split(',')[1].strip()
            commitment    = x[4]
            drafted       = x[5]
            debuted       = x[6]
            highschool    = x[7]
            travelteam    = x[8]
            playerRankingsList.append(BaseballEntity.BaseballEntity(rank, playerName, position,hometown,city, state, commitment, drafted,debuted,highschool,travelteam,"",height,weight, bat,throw, ProcessingYear))    
           except Exception as e:
                 print("Error")
    return playerRankingsList
def ScrapeBaseballRankingData(BaseURL, NumberOfPagesToScrape, ProcessingYear):
    # Create a list store the player rankings
    playerRankings = []

    # Get the first page of data.
    browserObj = mechanize.Browser()
    browserObj.addheaders = browserRequestHeaders #[('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]
    htmlResponse = browserObj.open(BaseURL)
    htmlSoupObj = BeautifulSoup(htmlResponse, parserType)
    playerRankings.append(ParseBaseballData(htmlSoupObj, ProcessingYear))


    # Get the html response from the BaseURL
    htmlResponse = browserObj.open(BaseURL)
    soup = BeautifulSoup(htmlResponse, parserType)
    # Begin scraping all pages of data from the website.
    for pg in range(2,NumberOfPagesToScrape):
        browserObj.select_form(nr=0) # the only form on the page
        browserObj.set_all_readonly(False) # to set the __doPostBack parameters

        for control in browserObj.form.controls[:]:
            if control.type in ['submit', 'image', 'checkbox']:
               control.disabled = True
        
        browserObj["__EVENTTARGET"] = "ctl00$ContentPlaceHolder1$gvPlayers" #next.group(1)
        browserObj["__EVENTARGUMENT"] = "Page${}".format(pg) #next.group(2)

        sleep(1)    
        htmlResponse = browserObj.submit()
        soup = BeautifulSoup(htmlResponse, parserType)
        playerRankings.append(ParseBaseballData(soup,ProcessingYear))
    
    return playerRankings

def main():
    PlayerRankings = []
    datasvc = DataServices()
    pagingData = datasvc.GetBaseballPagingDataBySport()
    for pageData in pagingData:
        BaseURL = "{}{}".format(pageData.Url, pageData.Year)
        print("")
        print("Processing Year {}".format(pageData.Year))
        PlayerRankings = ScrapeBaseballRankingData(BaseURL,pageData.NumberOfPages+1, pageData.Year)
        datasvc.SaveBaseballData(PlayerRankings)
        print("Completed processing year {}".format(pageData.Year))
        print("")



if __name__ == "__main__":
   t1_start = time.perf_counter()
   print("Start Date Time: {}".format(datetime.datetime.now()))
   main()
   t1_stop = time.perf_counter()
   print("End Date Time: {}".format(datetime.datetime.now()))
   print("Elapsed time:", t1_stop - t1_start)
