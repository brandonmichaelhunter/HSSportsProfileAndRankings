from dataclasses import dataclass

@dataclass
class BaseballEntity:
      rank: int
      name: str
      position: str
      hometown: str
      city:str
      state: str
      commitment: str
      drafted: str
      debuted: str
      highschool:str
      travelteam: str
      description: str
      height: str
      weight: str
      bats: str
      throw: str
      year: int